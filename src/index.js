const Web3 = require('web3')
const abi = require('../assets/Greeter.abi.json');
const { testnetNodeUrl } = require('../secrets')

const run = async () => {
    console.log("running");
    const web3 = new Web3(new Web3.providers.HttpProvider(testnetNodeUrl));
    const contractAddress = '0x0336168EC1041b9a8799E524DaaA97b57E9939E8';
    const greeter = new web3.eth.Contract(abi, contractAddress)

    const events = await greeter.getPastEvents('GreetingChanged', {
        fromBlock: "earliest",
        toBlock: "latest"
    });
    console.log(events);
    events.forEach(event => {
        console.log(`Greeting changed to: ${event.returnValues.newGreeting}`)
    })

}

run();
